#!/usr/bin/env python3
import rofi_menu


class WorkspaceItem(rofi_menu.Item):
    def __init__(self, workspace, i3, **kwargs):
        self.workspace = workspace
        self.i3 = i3

        flags = kwargs.get("flags", set())
        if workspace.focused:
            flags.add(rofi_menu.FLAG_STYLE_ACTIVE)

        kwargs["text"] = workspace.name
        kwargs["flags"] = flags
        super().__init__(**kwargs)

    def clone(self):
        return WorkspaceItem(self.workspace, self.i3)

    async def on_select(self, item_id, meta):
        await self.i3.command(f"workspace {self.workspace.name}")
        return rofi_menu.Operation(rofi_menu.OP_EXIT)


class I3Workspaces(rofi_menu.Menu):
    prompt = "workspaces"

    async def generate_menu_items(self, meta):
        from i3ipc.aio import Connection

        self.i3 = await Connection(auto_reconnect=True).connect()
        self.workspaces = await self.i3.get_workspaces()

        return [
            (w.name, WorkspaceItem(workspace=w, i3=self.i3)) for w in self.workspaces
        ]

    async def on_select(self, item_id, meta):
        if not item_id and meta.raw_input:
            await self.i3.command(f"workspace {meta.raw_input}")
            return rofi_menu.Operation(rofi_menu.OP_EXIT)

        return await super().on_select(item_id, meta)


class CounterItem(rofi_menu.Item):
    """Increment counter on selection"""

    async def load(self, meta):
        await super().load(meta)
        self.state = self.state or 0

    async def on_select(self, item_id, meta):
        self.state += 1
        return await super().on_select(item_id, meta)

    async def render(self, meta):
        return f"🏃 Selected #{self.state} time(s)"


class MainMenu(rofi_menu.Menu):
    prompt = "main menu"
    items = [
        rofi_menu.NestedMenu("Choose workspace", I3Workspaces()),
        CounterItem(),
    ]






import asyncio
from datetime import datetime
import os

import rofi_menu


class OutputSomeTextItem(rofi_menu.Item):
    """Output arbitrary text on selection"""
    async def on_select(self, meta):
        # any python code
        await asyncio.sleep(0.1)
        return rofi_menu.Operation(rofi_menu.OP_OUTPUT, (
            "💢 simple\n"
            "💥 multi-\n"
            "💫 <b>line</b>\n"
            "💣 <i>text</i>\n"
        ))


class DoAndExitItem(rofi_menu.Item):
    """Do something and exit"""
    async def on_select(self, meta):
        os.system('notify-send msg')
        return rofi_menu.Operation(rofi_menu.OP_EXIT)


class CurrentDatetimeItem(rofi_menu.Item):
    """Show current datetime inside menu item"""
    async def load(self, meta):
        self.state = datetime.now().strftime('%A %d. %B %Y (%H:%M:%S)')

    async def render(self, meta):
        return f"🕑 {self.state}"


class CounterItem(rofi_menu.Item):
    """Increment counter on selection"""
    async def load(self, meta):
        await super().load(meta)
        self.state = self.state or 0
        meta.session.setdefault("counter_total", 0)

    async def on_select(self, meta):
        self.state += 1
        meta.session["counter_total"] += 1
        return await super().on_select(meta)

    async def render(self, meta):
        per_menu_item = self.state
        total = meta.session["counter_total"]
        return f"🏃 Selected #{per_menu_item} time(s) (across menu items #{total})"


class HandleUserInputMenu(rofi_menu.Menu):
    allow_user_input = True

    class CustomItem(rofi_menu.Item):
        async def render(self, meta):
            entered_text = meta.session.get("text", "[ no text ]")
            return f"You entered: {entered_text}"

    items = [CustomItem()]

    async def on_user_input(self, meta):
        meta.session['text'] = meta.user_input
        return rofi_menu.Operation(rofi_menu.OP_REFRESH_MENU)


main_menu = rofi_menu.Menu(
    prompt="menu",
    items=[
        OutputSomeTextItem("Output anything"),
        DoAndExitItem("Do something and exit"),
        CurrentDatetimeItem(),
        CounterItem(),
        CounterItem(),
        rofi_menu.NestedMenu("User input", HandleUserInputMenu()),
    ],
)


if __name__ == "__main__":
    rofi_menu.run(main_menu)


# if __name__ == "__main__":
#     rofi_menu.run(MainMenu()),
