# Prototype


This is prototype in very, very early stage. API is by no means stable. Yet!



## Prereq: gRPC


```bash
export GO111MODULE=on  # Enable module mode
go get github.com/golang/protobuf/protoc-gen-go
```



## Install

Normal install
```bash
pip install .
```

Editable install
```bash
make user-install
```

Install CLI client
```bash
make go-cmd
```


Note: make sure you have `rofi` installed in your system


## Configure

Recommended i3-config entries

```bash
# start i3 daemon
exec_always --no-startup-id killall i3-flow-daemon; i3-flow-daemon

# Set i3flowctl commands location
set $flowctl ~/go/bin/i3flowctl


# Disable Group Moode
bindsym $mod+a exec $flowctl group none

# Change group
bindsym $mod+Control+grave exec $flowctl group "0: DEFAULT"
bindsym $mod+Control+1 exec $flowctl group "1: GROUP"
bindsym $mod+Control+2 exec $flowctl group "2: GROUP"
bindsym $mod+Control+3 exec $flowctl group "3: GROUP"
bindsym $mod+Control+4 exec $flowctl group "4: GROUP"

# Send workspace to group
bindsym $mod+Control+Shift+1 exec $flowctl group send-workspace "1: GROUP"
bindsym $mod+Control+Shift+2 exec $flowctl group send-workspace "2: GROUP"
bindsym $mod+Control+Shift+3 exec $flowctl group send-workspace "3: GROUP"
bindsym $mod+Control+Shift+4 exec $flowctl group send-workspace "4: GROUP"



# Change workspace
bindsym $mod+Control+Left exec $flowctl move LEFT
bindsym $mod+Control+Right exec $flowctl move RIGHT
bindsym $mod+Control+Shift+Left exec $flowctl move LEFT --move-container
bindsym $mod+Control+Shift+Right exec $flowctl move RIGHT --move-container

# activate prev / next
bindsym $mod+Control+Up exec $flowctl group prev
bindsym $mod+Control+Down exec $flowctl group next

bindsym $mod+Control+Shift+Up exec $flowctl group send-workspace "prev"
bindsym $mod+Control+Shift+Down exec $flowctl group send-workspace "next"


# Swap adjacent workspace
bindsym $mod+$alt+Left exec $flowctl swap LEFT
bindsym $mod+$alt+Right exec $flowctl swap RIGHT


# Create new workspace
bindsym $mod+n exec i3-flow-new-workspace
bindsym $mod+Shift+n exec i3-flow-new-workspace --move-container



# Rofi menu
bindsym $mod+g exec rofi -modi mymenu:i3-flow-rofi-groups -show mymenu
bindsym $mod+Shift+g exec rofi -modi mymenu:i3-flow-rofi-send-to-group -show mymenu
bindsym $mod+m exec rofi -modi mymenu:i3-flow-rofi-rename-group -show mymenu
```


## References and inspirations

Bellow is a list of other projects that were in one or another way inspiration for my work.

- [Regolith Linux](https://regolith-linux.org/): i3 for humans (Ubuntu based) - it's awesome, just go and check it
- [i3ipc-python](https://github.com/altdesktop/i3ipc-python): i3 ipc interface in Python
- [i3-workspace-groups](https://github.com/infokiller/i3-workspace-groups): Python based CLI tools for workspace group management
- [i3x3](https://github.com/seeruk/i3x3): Golang based management of workspaces organised in a 2D grid (client+daemon)
