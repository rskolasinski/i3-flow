from typing import List

import logging
import time
import i3ipc

from i3_flow.state import State
from i3_flow.helpers import Workspace, i3Proxy, get_inactive_name, get_active_name


logging.basicConfig(level=logging.DEBUG)


DIRECTIONS = ["LEFT", "RIGHT"]


class CommandError(Exception):
    pass


#### Helper Filters


def output_filter(active_output):
    return lambda ws: ws.ipc_data["output"] == active_output


def id_filter(workspace_ids):
    return lambda ws: ws.id in workspace_ids


#### Basic workspace Operations


def change_workspace(
    i3: i3ipc.Connection, state: State, direction: str, drag_container: bool
):
    logging.debug(f"Change workspace ({direction}) called.")
    target_ws = find_ws_in_direction(i3, state, direction)
    if target_ws is None:
        return
    i3Proxy(i3).change_workspace(target_ws.name, drag_container)


def swap_workspaces(i3: i3ipc.Connection, state: State, direction: str):
    logging.debug(f"Swap workspaces ({direction}) called.")
    source_ws = i3.get_tree().find_focused().workspace()
    target_ws = find_ws_in_direction(i3, state, direction)

    if target_ws is None:
        return

    ws1 = Workspace.from_i3_name(source_ws.name)
    ws2 = Workspace.from_i3_name(target_ws.name)

    new_ws_1 = Workspace(num=ws2.num, name=ws1.name, attrs=ws1.attrs)
    new_ws_2 = Workspace(num=ws1.num, name=ws2.name, attrs=ws2.attrs)

    i3Proxy(i3).rename_many([
        (source_ws.name, str(source_ws.id)),
        (target_ws.name, str(target_ws.id)),
        (str(source_ws.id), str(new_ws_1)),
        (str(target_ws.id), str(new_ws_2)),
    ])


#### Workspace helpers methods


def find_ws_in_direction(i3: i3ipc.Connection, state: State, direction: str):
    if direction not in DIRECTIONS:
        msg = f"{direction} not in allowed directions {DIRECTIONS}"
        raise CommandError(msg)

    i3p = i3Proxy(i3)

    active_group = state.output_group(i3p.active_output)

    logging.debug(f"active workspace {i3p.active_workspace.name}")
    logging.debug(f"active output {i3p.active_output}")
    logging.debug(f"active group: {active_group}")

    output_workspaces = filter(output_filter(i3p.active_output), i3p.workspaces)

    if active_group:
        workspace_ids = state.get_workspaces(active_group)
        workspaces = filter(id_filter(workspace_ids), output_workspaces)
    else:
        workspaces = output_workspaces

    if direction == "LEFT":
        workspaces = sorted(
            filter(lambda ws: ws.num < i3p.active_workspace.num, workspaces),
            key=lambda ws: ws.num,
            reverse=True,
        )
    elif direction == "RIGHT":
        workspaces = sorted(
            filter(lambda ws: ws.num > i3p.active_workspace.num, workspaces),
            key=lambda ws: ws.num,
        )

    logging.debug(f"Filtered workspaces: {[ws.name for ws in workspaces]}")

    if workspaces:
        return workspaces[0]
    else:
        return None


def find_unused_workspace(workspaces: List[i3ipc.Con]):
    used_numbers = sorted(set([ws.num for ws in workspaces]))
    free_number = next(i for i, e in enumerate(used_numbers + [None], 1) if i != e)
    return free_number


#### Workspace Groups Operations


def group_command(i3: i3ipc.Connection, state: State, command, group_name):
    logging.debug(f"GROUP_COMMAND: {command}")
    logging.debug(f"GROUP NAME: {group_name}")

    # logging.debug(f"GROUP STATE: {state}")

    if command == "ACTIVATE_GROUP":
        activate_group(i3, state, group_name, drag_workspace=False)
    elif command == "DISABLE_GROUP_MODE":
        activate_group(i3, state, None, drag_workspace=False)
    elif command == "ACTIVATE_NEXT":
        activate_adjacent_group(i3, state, forward=True, drag_workspace=False)
    elif command == "ACTIVATE_PREV":
        activate_adjacent_group(i3, state, forward=False, drag_workspace=False)
    elif command == "ASSIGN_WORKSPACE_TO_GROUP":
        if group_name not in ["prev", "next"]:
            activate_group(i3, state, group_name, drag_workspace=True)
        elif group_name == "next":
            activate_adjacent_group(i3, state, forward=True, drag_workspace=True)
        elif group_name == "prev":
            activate_adjacent_group(i3, state, forward=False, drag_workspace=True)
    elif command == "RENAME_GROUP":
        rename_group(i3, state, group_name)

    adjust_workspace_names(i3, state)
    # logging.debug(f"GROUP STATE: {state}")


def activate_group(
    i3: i3ipc.Connection, state: State, group_name, drag_workspace: bool
):
    i3p = i3Proxy(i3)

    state.assign_output_to_group(i3p.active_output, group_name)
    if group_name is None:
        return

    if drag_workspace:
        state.assign_group(i3p.active_workspace.id, group_name)
        return

    workspace_group = state.workspace_group(i3p.active_workspace.id)
    if workspace_group == group_name:
        return

    workspace_ids = state.get_workspaces(group_name)

    try:
        workspaces = filter(
            output_filter(i3p.active_output),
            filter(id_filter(workspace_ids), i3p.workspaces),
        )
        target_ws = sorted(
            workspaces, key=lambda ws: state.visit_time.get(ws.id, 0), reverse=True
        )[0]
        i3p.change_workspace(target_ws.name, drag_container=False)
    except IndexError:
        target_number = find_unused_workspace(i3p.workspaces)
        i3p.change_workspace(get_active_name(target_number), drag_container=False)

        # We changed workspace so we need to refresh tree
        active_workspace = i3p.drop_cache().active_workspace
        state.assign_group(active_workspace.id, group_name)


def activate_adjacent_group(
    i3: i3ipc.Connection, state: State, forward: bool, drag_workspace: bool
):
    i3p = i3Proxy(i3)

    output_workspaces = filter(output_filter(i3p.active_output), i3p.workspaces)
    output_groups = sorted(
        set([state.workspace_group(ws.id) for ws in output_workspaces])
    )
    active_group = state.output_group(i3p.active_output)

    if active_group is None:
        activate_group(i3, state, output_groups[0], drag_workspace)
        return

    assert active_group in output_groups

    logging.debug(f"ACTIVE GROUP: {active_group}")
    logging.debug(f"ALL GROUPS: {output_groups}")

    index = output_groups.index(active_group)
    if forward and index + 1 < len(output_groups):
        activate_group(
            i3, state, output_groups[index + 1], drag_workspace=drag_workspace
        )
    elif not forward and index - 1 >= 0:
        activate_group(
            i3, state, output_groups[index - 1], drag_workspace=drag_workspace
        )
    else:
        logging.debug("No group in desired direction.")


def rename_group(i3: i3ipc.Connection, state: State, group_name):
    i3p = i3Proxy(i3)
    active_group = state.output_group(i3p.active_output)
    state.rename_group(old_name=active_group, new_name=group_name)


#### Workspace events


def workspace_event(
    i3: i3ipc.Connection, state: State, event: i3ipc.events.WorkspaceEvent
):
    logging.debug(
        "PROCESSING WORKSPACE EVENT: "
        f"{event.change}: {event.old.name if event.old else None} -> {event.current.name}"
    )
    # logging.debug(f"GROUP STATE: {state}")

    if event.change == "init":
        workspace_init_event(i3, state, new_workspace=event.current)
    elif event.change == "focus":
        workspace_focus_event(i3, state, new_workspace=event.current)
    elif event.change == "empty":
        cleanup(i3, state)
    else:
        logging.error(f"Uknown i3 workspace event: {event.change}.")
        return

    adjust_workspace_names(i3, state)
    # logging.debug(f"GROUP STATE: {state}")


def workspace_init_event(i3: i3ipc.Connection, state: State, new_workspace: i3ipc.Con):
    i3p = i3Proxy(i3)
    active_group = state.output_group(i3p.active_output)
    state.assign_group(new_workspace.id, active_group)

    if active_group is None:
        return

    if len(list(filter(lambda ws: ws.num == new_workspace.num, i3p.workspaces))) > 1:
        target_number = find_unused_workspace(i3p.workspaces)
        i3p.rename(new_workspace.name, get_active_name(target_number))


def workspace_focus_event(i3: i3ipc.Connection, state: State, new_workspace: i3ipc.Con):
    active_output = new_workspace.ipc_data["output"]
    active_group = state.output_group(active_output)

    if active_group is None:
        return

    state.visit_time[new_workspace.id] = time.time()
    workspace_group = state.workspace_group(new_workspace.id)

    if workspace_group == active_group:
        return
    else:
        state.assign_output_to_group(active_output, workspace_group)


#### General helpers


def cleanup(i3: i3ipc.Connection, state: State):
    present_ids = [ws.id for ws in i3.get_tree().workspaces()]
    all_ids = state.group_assignments.keys()

    for ws_id in set(all_ids) - set(present_ids):
        del state.group_assignments[ws_id]


NUMBER_OFFSET = 1000


def adjust_workspace_names(i3: i3ipc.Connection, state: State):
    logging.debug("ADJUSTING WORKSPACE NAMES")

    i3p = i3Proxy(i3)

    output_workspaces = filter(output_filter(i3p.active_output), i3p.workspaces)
    active_group = state.output_group(i3p.active_output)

    if active_group:
        workspace_ids = state.get_workspaces(active_group)
        workspaces = list(filter(id_filter(workspace_ids), output_workspaces))
    else:
        workspaces = list(output_workspaces)

    desired_changes = []
    for number, ws in enumerate(workspaces, 1):
        new_name = get_active_name(number, ws.name)
        if new_name != ws.name:
            desired_changes.append((ws, new_name))

    for number, ws in enumerate(i3p.workspaces):
        if ws in workspaces:
            continue
        new_name = get_inactive_name(number + NUMBER_OFFSET, ws.name)

        if new_name != ws.name:
            desired_changes.append((ws, new_name))

    rename_tasks = []
    for ws, new_name in desired_changes:
        rename_tasks.append((ws.id, new_name))
        rename_tasks.insert(0, (ws.name, ws.id))

    # rename_tasks = []
    # for ws in output_workspaces:
    #     if ws.id in active_ids:
    #         new_name = get_active_name(ws.num)
    #     else:
    #         new_name = get_inactive_name(ws.num)
    #     if ws.name == new_name:
    #         continue
    #     else:
    #         rename_tasks.append([ws.name, new_name])

    i3p.rename_many(rename_tasks)
