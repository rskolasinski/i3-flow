#!/usr/bin/env python3

import os
import sys
import click
import threading
import logging
import i3ipc

from time import perf_counter, perf_counter_ns

from concurrent import futures
import grpc

# from i3_flow.operator import Operator
from i3_flow.state import init_state
from i3_flow.commands import (
    adjust_workspace_names,
    change_workspace,
    swap_workspaces,
    group_command,
    workspace_event,
)


from i3_flow.proto import i3flow_pb2
from i3_flow.proto import i3flow_pb2_grpc

SOCKET_FILE = "/tmp/i3-flow.sock"

logging.basicConfig(level=logging.DEBUG)


def on_shutdown(i3_conn, e):
    logging.debug("i3-flow shutting down")
    sys.exit(0)


def timeit(method):
    def timed(*args, **kwargs):
        t1 = perf_counter()
        result = method(*args, **kwargs)
        t2 = perf_counter()
        logging.debug(f"LATENCY of {method.__name__} is {1000 * (t2 - t1):.2f}")
        return result
    return timed


class GrpcDaemon(i3flow_pb2_grpc.GrpcDaemon):
    def __init__(self, i3, state, lock):
        self.i3 = i3
        self.state = state
        self.lock = lock

    @timeit
    def MoveWorkspace(self, request, context):
        direction = i3flow_pb2.Direction.Name(request.direction)
        move_container = request.move_container
        change_workspace(self.i3, self.state, direction, move_container)
        return i3flow_pb2.CommandReply(success=True)

    @timeit
    def SwapWorkspace(self, request, context):
        direction = i3flow_pb2.Direction.Name(request.direction)
        with self.lock:
            swap_workspaces(self.i3, self.state, direction)
        return i3flow_pb2.CommandReply(success=True)

    @timeit
    def GroupControl(self, request, context):
        with self.lock:
            group_command(
                self.i3,
                self.state,
                command=i3flow_pb2.COMMAND.Name(request.command),
                group_name=request.group_name,
            )
        return i3flow_pb2.CommandReply(success=True)

    @timeit
    def StateInfo(self, request, context):
        return i3flow_pb2.StateInfoReply(json=self.state.json())

    @staticmethod
    def get_grpc_server(i3, state, lock):
        grpc_daemon = GrpcDaemon(i3, state, lock)
        server = grpc.server(futures.ThreadPoolExecutor(max_workers=2))
        i3flow_pb2_grpc.add_GrpcDaemonServicer_to_server(grpc_daemon, server)
        server.add_insecure_port(f"unix://{SOCKET_FILE}")
        return server


class Daemon:
    def __init__(self):
        self.lock = threading.RLock()

        self.i3 = i3ipc.Connection(auto_reconnect=True)
        self.state = init_state(self.i3)

        adjust_workspace_names(self.i3, self.state)

        self.i3.on(i3ipc.Event.WORKSPACE, self.on_workspace_event)
        self.i3.on("shutdown", on_shutdown)

        if os.path.exists(SOCKET_FILE):
            os.remove(SOCKET_FILE)

        # self.operator = Operator(self.i3)
        self.grpc_server = GrpcDaemon.get_grpc_server(self.i3, self.state, self.lock)

    def on_workspace_event(self, i3, event):
        with self.lock:
            workspace_event(self.i3, self.state, event)

    def launch_i3(self):
        self.i3.main()

    def run(self):
        t_i3 = threading.Thread(target=self.launch_i3)
        t_i3.start()
        self.grpc_server.start()


@click.command()
def main():
    watcher = Daemon()
    watcher.run()
