import click
import logging

import i3ipc

from i3_flow.commands import find_unused_workspace

logging.basicConfig(level=logging.DEBUG)


DIRECTIONS = ["LEFT", "RIGHT"]


i3 = i3ipc.Connection(auto_reconnect=True)


# def debug_current_workspaces():
#     data = [dict(num=w.num, name=w.name, output=w.output) for w in i3.get_workspaces()]
#     from pprint import pprint
#     pprint(data)


def change_workspace(number, move_container):
    cmds = [f"workspace number {number}"]
    if move_container:
        cmds.insert(0, f"move container to workspace {number}")
    cmd = ";".join(cmds)
    logging.debug(f"cmd: {cmd}")
    cr = i3.command(cmd)
    logging.debug(f"res: {[c.ipc_data for c in cr]}")


@click.command()
@click.option("--move-container", is_flag=True)
def new_workspace(move_container):
    # debug_current_workspaces()
    logging.debug(f"move_container: {move_container}")

    target = find_unused_workspace(i3.get_workspaces())
    change_workspace(target, move_container)
