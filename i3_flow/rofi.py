import rofi_menu

import logging

logging.basicConfig(level=logging.DEBUG)


async def current_group(i3):
    import grpc

    from google.protobuf.empty_pb2 import Empty

    from i3_flow.proto import i3flow_pb2_grpc

    from i3_flow.state import State
    from i3_flow.daemon import SOCKET_FILE

    with grpc.insecure_channel(f"unix://{SOCKET_FILE}") as channel:
        stub = i3flow_pb2_grpc.GrpcDaemonStub(channel)
        response = stub.StateInfo(Empty())

    state = State.parse_raw(response.json)

    i3_tree = await i3.get_tree()

    active_workspace = i3_tree.find_focused().workspace()
    active_group = state.workspace_group(active_workspace.id)
    return active_group


async def groups_on_output(i3):
    import grpc

    from google.protobuf.empty_pb2 import Empty

    from i3_flow.proto import i3flow_pb2_grpc

    from i3_flow.state import State
    from i3_flow.daemon import SOCKET_FILE

    from i3_flow.commands import output_filter

    with grpc.insecure_channel(f"unix://{SOCKET_FILE}") as channel:
        stub = i3flow_pb2_grpc.GrpcDaemonStub(channel)
        response = stub.StateInfo(Empty())

    state = State.parse_raw(response.json)

    i3_tree = await i3.get_tree()

    active_workspace = i3_tree.find_focused().workspace()
    active_output = active_workspace.ipc_data["output"]

    output_workspaces = list(filter(output_filter(active_output), i3_tree.workspaces()))

    active_group = state.workspace_group(active_workspace.id)
    groups = sorted(set([state.workspace_group(ws.id) for ws in output_workspaces]))

    return groups, active_group


def activate_group(group_name):
    import grpc

    from i3_flow.proto import i3flow_pb2, i3flow_pb2_grpc
    from i3_flow.daemon import SOCKET_FILE

    with grpc.insecure_channel(f"unix://{SOCKET_FILE}") as channel:
        stub = i3flow_pb2_grpc.GrpcDaemonStub(channel)
        return stub.GroupControl(
            i3flow_pb2.GroupCommand(
                command=i3flow_pb2.COMMAND.Value("ACTIVATE_GROUP"),
                group_name=group_name,
            )
        )


def rename_group(group_name):
    import grpc

    from i3_flow.proto import i3flow_pb2, i3flow_pb2_grpc
    from i3_flow.daemon import SOCKET_FILE

    with grpc.insecure_channel(f"unix://{SOCKET_FILE}") as channel:
        stub = i3flow_pb2_grpc.GrpcDaemonStub(channel)
        return stub.GroupControl(
            i3flow_pb2.GroupCommand(
                command=i3flow_pb2.COMMAND.Value("RENAME_GROUP"), group_name=group_name,
            )
        )


def send_to_group(group_name):
    import grpc

    from i3_flow.proto import i3flow_pb2, i3flow_pb2_grpc
    from i3_flow.daemon import SOCKET_FILE

    with grpc.insecure_channel(f"unix://{SOCKET_FILE}") as channel:
        stub = i3flow_pb2_grpc.GrpcDaemonStub(channel)
        return stub.GroupControl(
            i3flow_pb2.GroupCommand(
                command=i3flow_pb2.COMMAND.Value("ASSIGN_WORKSPACE_TO_GROUP"),
                group_name=group_name,
            )
        )


class GroupItem(rofi_menu.Item):
    def __init__(self, group_name, active, drag_workspace=False, **kwargs):
        self.group_name = group_name
        self.active = active
        self.drag_workspace = drag_workspace

        flags = kwargs.get("flags", set())
        if self.active:
            flags.add(rofi_menu.FLAG_STYLE_ACTIVE)

        kwargs["text"] = group_name
        kwargs["flags"] = flags
        super().__init__(**kwargs)

    def clone(self):
        return GroupItem(self.group_name, self.active, self.drag_workspace)

    async def on_select(self, item_id, meta):
        if not self.drag_workspace:
            _ = activate_group(self.group_name)
        else:
            _ = send_to_group(self.group_name)
        return rofi_menu.Operation(rofi_menu.OP_EXIT)


class GroupsMenu(rofi_menu.Menu):
    prompt = "select group"

    def __init__(self, drag_workspace=False, **kwargs):
        self.drag_workspace = drag_workspace
        if self.drag_workspace:
            self.prompt = "send workspace to group"
        super().__init__(**kwargs)

    async def generate_menu_items(self, prefix_path, meta):
        from i3ipc.aio import Connection

        self.i3 = await Connection(auto_reconnect=True).connect()
        groups, active_group = await groups_on_output(self.i3)

        return [
            (name, GroupItem(name, name == active_group, self.drag_workspace))
            for name in groups
        ]

    async def on_select(self, item_id, meta):
        if not item_id and meta.raw_input:
            new_name = meta.raw_input
            if not self.drag_workspace:
                _ = activate_group(new_name)
            else:
                _ = send_to_group(new_name)
            return rofi_menu.Operation(rofi_menu.OP_EXIT)

        return await super().on_select(item_id, meta)


class RenameGroup(rofi_menu.Menu):
    prompt = "choose new name"

    async def on_select(self, item_id, meta):
        if not item_id and meta.raw_input:
            new_name = meta.raw_input
            logging.debug(f"Choosing New Group Name: {new_name}")
            rename_group(new_name)
            return rofi_menu.Operation(rofi_menu.OP_EXIT)

        return await super().on_select(item_id, meta)


def rename_workspace(new_name):
    from i3ipc import Connection

    i3 = Connection(auto_reconnect=True)
    ws = i3.get_tree().find_focused().workspace()

    new_full_name = (
        f"{ws.num}:<span font_desc='JetBrains Mono Medium 13'>{new_name}</span>"
    )
    i3.command(f'rename workspace to "{new_full_name}"')


class RenameWorkspace(rofi_menu.Menu):
    prompt = "Choose new workspace name"

    async def on_select(self, item_id, meta):
        if not item_id and meta.raw_input:
            new_name = meta.raw_input
            logging.debug(f"Choosing New Workspace Name: {new_name}")
            rename_workspace(new_name)
            return rofi_menu.Operation(rofi_menu.OP_EXIT)

        return await super().on_select(item_id, meta)


def groups_menu():
    rofi_menu.run(GroupsMenu())


def send_to_group_menu():
    rofi_menu.run(GroupsMenu(drag_workspace=True))


def rename_group_menu():
    rofi_menu.run(RenameGroup())


def rename_workspace_menu():
    rofi_menu.run(RenameWorkspace())


class WorkspaceItem(rofi_menu.Item):
    def __init__(self, workspace, i3, **kwargs):
        self.workspace = workspace
        self.i3 = i3

        flags = kwargs.get("flags", set())
        if workspace.focused:
            flags.add(rofi_menu.FLAG_STYLE_ACTIVE)

        kwargs["text"] = workspace.name
        kwargs["flags"] = flags
        super().__init__(**kwargs)

    def clone(self):
        return WorkspaceItem(self.workspace, self.i3)

    async def on_select(self, item_id, meta):
        await self.i3.command(f"workspace {self.workspace.name}")
        return rofi_menu.Operation(rofi_menu.OP_EXIT)


class I3Workspaces(rofi_menu.Menu):
    prompt = "workspaces"

    async def generate_menu_items(self, prefix_path, meta):
        from i3ipc.aio import Connection

        self.i3 = await Connection(auto_reconnect=True).connect()
        self.workspaces = await self.i3.get_workspaces()

        return [
            (ws.name, WorkspaceItem(workspace=ws, i3=self.i3)) for ws in self.workspaces
        ]

    async def on_select(self, item_id, meta):
        if not item_id and meta.raw_input:
            await self.i3.command(f"workspace {meta.raw_input}")
            return rofi_menu.Operation(rofi_menu.OP_EXIT)

        return await super().on_select(item_id, meta)


class MainMenu(rofi_menu.Menu):
    prompt = "i3-fow Main Menu"
    items = [
        rofi_menu.NestedMenu("Activate Group", GroupsMenu()),
        # rofi_menu.NestedMenu("Rename Group", RenameGroup()),
        rofi_menu.NestedMenu("Activate Workspace", I3Workspaces()),
    ]


def main():
    rofi_menu.run(MainMenu())
