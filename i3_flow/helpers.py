from __future__ import annotations

from typing import List, Tuple

from bs4 import  BeautifulSoup
from pydantic import BaseModel

import logging
import i3ipc

from backports.cached_property import cached_property


logging.basicConfig(level=logging.DEBUG)

I3_CMDS = {
    "WORKSPACE_CHANGE": "workspace {name}",
    "MOVE_CONTAINER": "move container to workspace {name}",
    "RENAME_COMMAND": 'rename workspace "{old_name}" to "{new_name}"',
}


GLYPH = ""
# INACTIVE_COLOUR = "#2E3440"
INACTIVE_COLOUR = "#7B8394"


class Attrs(BaseModel):
    font_desc: str = None
    foreground: str = None


class Workspace(BaseModel):
    num: int
    name: str
    attrs: Attrs = Attrs()

    def __str__(self):
        attrs = {k: v for (k, v) in dict(self.attrs).items() if v is not None}
        if attrs:
            attrs = " ".join([f"{k}='{v}'" for (k, v) in attrs.items()])
            return f"{self.num}:<span {attrs}>{self.name}</span>"
        else:
            return f"{self.num}:{self.name}"

    @staticmethod
    def from_i3_name(i3_name: str) -> Workspace:
        if ":" in i3_name:
            num, name = i3_name.split(':', 1)
        else:
            num, name = i3_name, ""
        name, attrs = Workspace.decode_name(name)
        return Workspace(num=num, name=name, attrs=attrs)

    @staticmethod
    def decode_name(name: str) -> Tuple[str, Attrs]:
        soup = BeautifulSoup(name, features="html.parser")
        span = soup.find("span")
        if span:
            return span.text, Attrs(**span.attrs)
        else:
            return soup.text, Attrs()



def get_active_name(number: int, i3_name: str = None) -> str:
    if i3_name == None:
        i3_name = f"{number}:{GLYPH}"
    ws = Workspace.from_i3_name(i3_name)
    ws.num = number
    ws.attrs.foreground = None
    return str(ws)


def get_inactive_name(number: int, i3_name: str = None) -> str:
    if i3_name == None:
        i3_name = f"{number}:{GLYPH}"
    ws = Workspace.from_i3_name(i3_name)
    ws.num = number
    ws.attrs.foreground = INACTIVE_COLOUR
    return str(ws)


class i3Proxy:
    """Proxy to i3.

    This class will cache once fetched tree until `drop_cache` is called or
    a command known to alter tree is called.
    """

    _NOT_CACHED_ITEMS = ["i3"]

    def __init__(self, i3: i3ipc.Connection = None):
        self.i3 = i3ipc.Connection(auto_reconnect=True) if i3 is None else i3

    def drop_cache(self) -> i3Proxy:
        """Drop cache (in-place) and return copy of self (for chaining commands)."""
        logging.debug("dropping cache")
        self.__dict__ = {item: self.__dict__[item] for item in self._NOT_CACHED_ITEMS}
        return self

    @cached_property
    def i3_tree(self) -> i3ipc.Con:
        """Return the i3 tree."""
        logging.debug("computing: self.i3_tree <- self.i3.get_tree()")
        return self.i3.get_tree()

    @cached_property
    def workspaces(self) -> List[i3ipc.Con]:
        """Return all workspaces."""
        logging.debug("computing: self.workspaces <- self.i3_tree.workspaces()")
        return self.i3_tree.workspaces()

    @cached_property
    def focused(self) -> i3ipc.Con:
        """Return currently focused container."""
        logging.debug("computing: self.focused <- self.i3_tree.find_focused()")
        return self.i3_tree.find_focused()

    @cached_property
    def active_workspace(self) -> i3ipc.Con:
        """Return current workspace."""
        logging.debug("computing: self.active_workspace <- self.focused.workspace()")
        return self.focused.workspace()

    @cached_property
    def active_output(self) -> str:
        """Return current output."""
        return self.active_workspace.ipc_data["output"]

    def exec_cmds(self, cmds: List[str]) -> List[dict]:
        """Exec multimple commands."""
        cmd = ";".join(cmds)
        cr = self.i3.command(cmd)
        logging.debug(f"cmds: {cmds}")
        logging.debug(f"res: {[c.ipc_data for c in cr]}")
        return cr

    def change_workspace(self, name: str, drag_container: bool = False):
        cmds = [I3_CMDS["WORKSPACE_CHANGE"].format(name=name)]
        if drag_container:
            cmds.insert(0, I3_CMDS["MOVE_CONTAINER"].format(name=name))
        self.exec_cmds(cmds)
        self.drop_cache()

    def rename(self, old_name: str, new_name: str):
        self.rename_many([old_name, new_name])

    def rename_many(self, rename_tasks: List[Tuple[str, str]]):
        cmds = [
            I3_CMDS["RENAME_COMMAND"].format(old_name=old_name, new_name=new_name)
            for (old_name, new_name) in rename_tasks
        ]
        self.exec_cmds(cmds)
        self.drop_cache()

    def swap(self, ws_a, ws_b):
        data = [
            (ws_a.name, str(ws_a.id)),
            (ws_b.name, ws_a.name),
            (str(ws_a.id), ws_b.name),
        ]
        self.rename_many(data)
        self.drop_cache()
