package cmd

import (
	"fmt"
	pb "gitlab.com/rskolasinski/i3-flow/cmd/proto"
)

func getDirection(direction string) (pb.Direction, error) {

	value, ok := pb.Direction_value[direction]
	if !ok {
		return pb.Direction(0), fmt.Errorf("Invalid direction: %v\n", direction)
	}

	return pb.Direction(value), nil
}
