package cmd

import (
	"github.com/spf13/cobra"
	"log"
)

var socketFile = "/tmp/i3-flow.sock"

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "i3flowctl",
	Short: "i3flowctl controls i3-flow-daemon",
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		log.Fatal(err)
	}
}

func init() {
	rootCmd.AddCommand(moveCmd)
	rootCmd.AddCommand(swapCmd)
}
