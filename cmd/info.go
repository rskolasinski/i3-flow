package cmd

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/spf13/cobra"
	"google.golang.org/grpc"

	"github.com/golang/protobuf/ptypes/empty"

	pb "gitlab.com/rskolasinski/i3-flow/cmd/proto"
)

func init() {
	rootCmd.AddCommand(infoCmd)
}

var infoCmd = &cobra.Command{
	Use:   "info",
	Short: "get state information",
	Args:  cobra.ExactArgs(0),
	Run: func(cmd *cobra.Command, args []string) {

		// Set up a connection to the server.
		address := "unix://" + socketFile
		conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithBlock(), grpc.WithTimeout(100*time.Millisecond))
		if err != nil {
			log.Fatalf("did not connect: %v", err)
		}
		defer conn.Close()
		c := pb.NewGrpcDaemonClient(conn)

		// Contact the server and print out its response.
		ctx, cancel := context.WithTimeout(context.Background(), time.Second)
		defer cancel()

		r, err := c.StateInfo(ctx, &empty.Empty{})

		if err != nil {
			log.Fatalf("could not greet: %v", err)
		}
		fmt.Println(r.GetJson())
	},
}
